const express = require("express")
const router = express.Router()
const ProductsService = require("../Services/ApiML")

const productsService = new ProductsService()


// Get Item List
router.get("/", async function(req, res, next) {
  let searchString = req.query.q
  try {
    const products = await productsService.getProductsList({searchString})
    res.send(
      {
          "author":{
          "name": "Giannina",
          "lastname": "Calcagno"},
          data: products,
          message: "products listed"
      }
  )
  } catch (err) {
    next(err)
  }
})



// Get Item Data
router.get("/:id", async function(req, res, next) {
  const { id } = req.params
  try {
    const product = await productsService.getProduct({id})
    res.send({
        "author":{
        "name": "Giannina",
        "lastname": "Calcagno"
      },
      data: product,
        message: "product retrieved"
    })
  } catch (err) {
    next(err)
  }
})

module.exports = router